"use strict";
/*
Функция - это отдельный кусок кода, подпрограмма, которая встраивается в программу. Функции нужны для облегчения работы, упрощения и сокращения кода. Чтобы при вызове функции она выполняла свою работу, в нее нужно вставить аргументы, с которыми эта функция будет делать вычисления, действия.
*/
let firstNum = "";
let secondNum = "";
let mathSymb = "";
while (!Number.isFinite(firstNum) || !Number.isFinite(secondNum)) {
    firstNum = +prompt("Please enter the first number: ", "");
    secondNum = +prompt("Please enter the second number: ", "");
    if (!Number.isFinite(firstNum) || !Number.isFinite(secondNum)) {
        alert("Error! Please be more attentive this time while entering numbers.");
    }
}
while (mathSymb !== "+" && mathSymb !== "-" && mathSymb !== "/" && mathSymb !== "*") {
    mathSymb = prompt("Enter the symbol of the math operation '+' or '-' or '/' or '*'");
}
function calculateTwoNum (a, b, symb) {
    return eval(a + symb + b);
}
const res = calculateTwoNum(firstNum, secondNum, mathSymb);
console.log(`${firstNum} ${mathSymb} ${secondNum} = ${res}`)